import mongojs from 'mongojs'
import { mongoConfig, collections } from './index'

let { username, ip, port, dbName, password } = mongoConfig

const db = mongojs(`mongodb://${username}:${encodeURIComponent(password)}@${ip}:${port}/${dbName}`, collections);
// const db = mongojs(`mongodb://127.0.0.1:27017/fms`, collections);

export default db;