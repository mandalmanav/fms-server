import db from '../../config/db'
import validate from '../../helpers/fieldValidation'

import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'
import isAuth from '../../helpers/isAuth';
import mongojs from 'mongojs'
export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!validate(data, ["_id"])) {
        badRequest(response)
    }
    else{
        isAuth(request).then((data) => {
            query["vendorId"] = data._id;
           db.orders.find(query).sort({ createdOn: -1 }, (err, docs) => {
                if (err) {
                    internalServerError(response)
                }
                else {
                    success(response, docs)
                }
            })
        })
            .catch((err) => {
                unAuthorized(response)
            })
    
        }
    }
    
    