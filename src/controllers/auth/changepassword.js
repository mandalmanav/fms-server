import mongojs from "mongojs";
import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import {
    success,
    internalServerError,
    badRequest,
    conflict,
    forbidden,
    unAuthorized
} from "../../config/responsetemplate";
import isAuthed from "../../helpers/isAuth";
import isSuperAdmin from "../../helpers/isSuperAdmin";
import { tokenConfig } from "../../config/index";
import jwt from "jsonwebtoken";
export default (request, response) => {
    let query = {};
    let { data } = request.body;
    if (!validate(data, ["newPassword"])) {
        badRequest(response);
    } else {
        isAuthed(request)
            .then((user) => {
                db.vendors.find({
                    _id: mongojs.ObjectId(user._id),
                    password: data.password
                }, (_err, _doc) => {
                    if (_err) {
                        internalServerError(response)
                    }
                    else {
                        if (_doc.length) {
                            db.vendors.update({
                                _id: mongojs.ObjectId(user._id)
                            }, {
                                    $set: {
                                        password: data.newPassword
                                    }
                                }, (err, doc) => {
                                    if (err) {
                                        internalServerError(response)
                                    }
                                    else {
                                        success(response, {
                                            message: 'password updated'
                                        })
                                    }
                                })
                        }
                        else {
                            forbidden(response, {
                                message: "Current password is incorrect"
                            })
                        }
                    }
                })

            })
            .catch((err) => {
                console.log(err);
                unAuthorized(response)
            })

    }
};
