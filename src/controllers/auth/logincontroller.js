import mongojs from "mongojs";
import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import {
  success,
  internalServerError,
  badRequest,
  conflict,
  forbidden,
  unAuthorized
} from "../../config/responsetemplate";
import isAuthed from "../../helpers/isAuth";
import isSuperAdmin from "../../helpers/isSuperAdmin";
import { tokenConfig } from "../../config/index";
import jwt from "jsonwebtoken";

export default (request, response) => {
  let query = {};
  let { data } = request.body;
  if (!validate(data, ["email", "password"])) {
    badRequest(response);
  } else {
    console.log(data);
    db.vendors.find(
      data,
      // {
      //   _id: 1,
      //   email: 1,
      //   mobile: 1,
      //   regName: 1,
      //   createdOn: 1,
      //   modifiedOn: 1,
      //   status: 1,
      //   role: 1
      // },
      (err, doc) => {
        if (err) {
          console.log(err);
          internalServerError(response);
        } else {
          console.log(doc);
          if (doc.length) {
           // if (!(doc[0].status == "approved"||doc[0].status == "active" || doc[0].status == "sleep")) {
           if(false){
           unAuthorized(response, { message: "Pending for approval" });
            } else {
              delete doc[0].password
              delete doc[0].exp
              delete doc[0].iat
              
              var refreshToken = jwt.sign(
                doc[0],
                tokenConfig.refreshToken.secret,
                {
                  expiresIn: tokenConfig.refreshToken.expiresIn
                }
              );
              var token = jwt.sign(doc[0], tokenConfig.token.secret, {
                expiresIn: tokenConfig.token.expiresIn
              });

              success(response, {
                userDetails: doc[0],
                refreshToken,
                token
              });
            }
          } else
            unAuthorized(response, { message: "Invalid username or password" });
        }
      }
    );
  }
};
