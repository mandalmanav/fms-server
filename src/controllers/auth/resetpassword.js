import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'

export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!validate(data, ["mobile"])) {
        badRequest(response)
    }
    else {
        db.vendors.find({
            mobile:data.mobile
        }, {
            "_id": 1,
            "email": 1,
            "mobile": 1,
            "regName": 1,
            "createdOn": 1,
            "modifiedOn": 1,
            "status": 1,
            "role": 1
        }, (err, doc) => {
            if (err) {
                internalServerError(response)
            }
            else {
                if (doc.length) {
                    if (doc[0].status != 'active') {
                        forbidden(response, { message: "suspended" })
                    }
                    else {
                        
                        success(response, {
                            message:'otp sent'
                        })
                    }

                }
                else
                    unAuthorized(response, { message: "user not found" })
            }
        })
    }
}