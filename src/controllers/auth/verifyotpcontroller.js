import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'
import verifyotp from '../../helpers/verifyotp';
import mongojs from 'mongojs'
export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!validate(data, ["otp", "mobile", "vendorId"])) {
        badRequest(response)
    }
    else {
        verifyotp(`91${data.mobile}`, data.otp)
            .then((otpres) => {
                if (otpres.type == "success" && otpres.message == "otp_verified") {
                    db.vendors.update({
                        _id: mongojs.ObjectId(data["vendorId"])
                    }, {
                            $set: {
                                status: 'verified'
                            }
                        }, (err, doc) => {
                            if (err) {
                                internalServerError(response)
                            }
                            else {
                                success(response, otpres)
                            }
                        })
                }
                else {
                    success(response, otpres)
                }

            })
            .catch((otp_err) => {
                internalServerError(response, otp_err)
            })
    }
}