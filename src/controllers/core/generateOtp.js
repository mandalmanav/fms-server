import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";

import {
  success,
  internalServerError,
  badRequest,
  conflict,
  unAuthorized
} from "../../config/responsetemplate";
import generateotp from "../../helpers/generateotp";

export default (request, response) => {
  let query = {};
  let {
    data
  } = request.body;
  if (!validate(data, [
    "mobile"
   ])) {
   badRequest(response);
 } 
 else {
    generateotp(data.mobile).then((data)=>{
        success(response,data)
    }).catch((err)=>{
        internalServerError(response,err)
    })
 }
};
