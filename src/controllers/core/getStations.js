import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";
import Axios from 'axios'
import {
  success,
  internalServerError,
  badRequest,
  conflict,
  unAuthorized
} from "../../config/responsetemplate";
import generateotp from "../../helpers/generateotp";

export default (request, response) => {
    Axios.get(`https://www.foodtrainonline.com/api/api_rr_v3_test.php/api_rr_v3_test.php?train_no=${request.params.trainNo}&page_type=plain_train_route`)
    .then((result)=>{
        success(response,result.data.mainJson)
    })
    .catch((err)=>{
        internalServerError(response)
        
    })
}