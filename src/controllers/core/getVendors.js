import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";
import Axios from 'axios'
import mongojs from 'mongojs'
import {
  success,
  internalServerError,
  badRequest,
  conflict,
  unAuthorized
} from "../../config/responsetemplate";

export default (request, response) => {
    let code = request.params.code
    console.log(code);
    db.outlets.find({
        stations:code
    },(err,docs)=>{
        if(err){
            internalServerError(response)
        }
        else{
            let vendorIds = docs.map((doc)=>mongojs.ObjectId(doc.vendorId))
            db.vendors.find({
                _id:{
                    $in:vendorIds
                },
                status:"active"
            },{
                regName:1,
                _id:1
            },(err_,docs_)=>{
                if(err_){

                    internalServerError(response,err_)
                }
                else
                {
                    success(response,docs_)
                }
            })
        }
    })

}