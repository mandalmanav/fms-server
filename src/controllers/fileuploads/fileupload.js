import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";
import { unAuthorized } from "../../config/responsetemplate";
const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
export default (request, response) => {
isAuth(request).then(user=>{
    const spacesEndpoint = new aws.Endpoint('sgp1.digitaloceanspaces.com');
    const s3 = new aws.S3({
        endpoint: spacesEndpoint
    });
    let dateTime = Date.now()
    let fileName=""
    const upload = multer({
        storage: multerS3({
            s3: s3,
            bucket: 'fms-storage/items',
            acl: 'public-read',
            key: function (request, file, cb) {
                fileName=  user._id+"-"+dateTime+"-"+file.originalname
                cb(null, fileName);
            }
        })
    }).array('upload', 1);

    upload(request, response, function (error) {
        
        if (error) {
            console.log(error);
            return response.send("error");
        }
        console.log('File uploaded successfully.');
        response.send(fileName);
    });
})
.catch((err)=>{
    console.log(err);
    unAuthorized(respones)
})


  
}
