import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";
import {
  success,
  internalServerError,
  badRequest,
  conflict,
  unAuthorized
} from "../../config/responsetemplate";

export default (request, response) => {
  let query = {};
  let {
    data
  } = request.body;
  isAuth(request)
    .then(user => {
      if (!validate(data, [
          "name",
          "cuisine",
          "price",
          "deliverySLA",
          "opensAt",
          "closesAt",
          "tags"
        ])) {
        badRequest(response);
      } else {
        console.log(user);
        query["name"] = data.name;
        query["cuisine"] = data.cuisine;
        query["price"] = data.price;
        query["description"] = data.description;
        query["deliverySLA"] = data.deliverySLA;
        query["image"] = data.image;
        query["opensAt"] = data.opensAt;
        query["closesAt"] = data.closesAt;
        query["createdOn"] = Date.now();
        query["modifiedOn"] = Date.now();
        query["status"] = "active";
        query["vendorId"] = user._id;
        query["tags"] = data.tags;

        db.items.insert(query, (err, doc) => {
          if (err) {
            internalServerError(response);
          } else {
            success(response, {
              _id: doc._id
            });
          }
        });
      }
    })
    .catch(err => {
      unAuthorized(response);
    });
};
