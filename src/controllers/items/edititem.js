import mongojs from "mongojs";
import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";
import {
  success,
  internalServerError,
  badRequest,
  conflict,
  unAuthorized
} from "../../config/responsetemplate";

export default (request, response) => {
  let query = {};
  let { data } = request.body;
  isAuth(request)
    .then(user => {
      if (
        !validate(data, [
          "itemId",
          "name",
          "cuisine",
          "price",
          "deliverySLA",
          "opensAt",
          "closesAt",
          "vendorId",
          "tags"
        ])
      ) {
        badRequest(response);
      } else {
        query["name"] = data.name;
        query["cuisine"] = data.cuisine;
        query["price"] = data.price;
        query["description"] = data.description;
        query["deliverySLA"] = data.deliverySLA;
        query["image"] = data.image;
        query["opensAt"] = data.opensAt;
        query["closesAt"] = data.closesAt;
        query["modifiedOn"] = Date.now();
        query["status"] = data.status;
        query["tags"] = data.tags;

        db.items.update(
          {
            _id: mongojs.ObjectId(data["itemId"])
          },
          {
            $set: {
              ...query
            }
          },
          (err, doc) => {
            if (err) {
              internalServerError(response);
            } else {
              success(response, doc);
            }
          }
        );
      }
    })
    .catch(() => {
      unAuthorized(response);
    });
};
