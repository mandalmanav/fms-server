import db from '../../../config/db'
import validate from '../../../helpers/fieldValidation'
import isAuthed from '../../../helpers/isAuth'
import isSuperAdmin from '../../../helpers/isSuperAdmin'
import { success, internalServerError, badRequest, conflict } from '../../../config/responsetemplate'


export default (request, response) => {
    let query = {}
    let { data } = request.body
    // if (!isAuthed()) {
    //     unAuthorized(response)
    //     return
    // }
    // if (!isSuperAdmin()) {
    //     forbidden(response)
    //     return
    // }
    if (!validate(data, ["name", "status"])) {
        badRequest(response)
    }
    else {

                    db.cuisines.insert({
                        
                        name: data.name,
                        status: data.status

                    }, (err, doc) => {
                        if (err) {
                            conflict(response)
                        }
                        else {
                            success(response, { _id: doc._id })
                        }
                    })
             

    }

}