import mongojs from 'mongojs'
import db from '../../../config/db'
import validate from '../../../helpers/fieldValidation'
import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../../config/responsetemplate'
import isAuthed from '../../../helpers/isAuth'
import isSuperAdmin from '../../../helpers/isSuperAdmin'
export default (request, response) => {
    let query = {}
    let { data } = request.body
    // if (!isAuthed()) {
    //     unAuthorized(response)
    //     return
    // }
    // if (!isSuperAdmin()) {
    //     forbidden(response)
    //     return
    // }
    if (!validate(data, ["cuisineId","name","status"])) {
        badRequest(response)
    }
    else {
        
        db.cuisines.update({
            "_id": mongojs.ObjectId(data["cuisineId"])
        }, {
                $set:{
                    "name":data.name,
                    "status":data.status
                }
            }, (err, doc) => {
                if (err) {
                    internalServerError(response)
                }
                else {
                     success(response, doc)
                }
            })
    }
}