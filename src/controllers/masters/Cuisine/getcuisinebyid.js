
import db from '../../../config/db'
import validate from '../../../helpers/fieldValidation'
import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../../config/responsetemplate'
export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!validate(data, ["_id"])) {
        badRequest(response)
    }
    else {
        db.cuisines.find({
            
            "_id":data._id
            
        }, (err, doc) => {
                if (err) {
                    internalServerError(response)
                }
                else {
                    success(response, doc)
                }
            })
    }
}

