import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";
import mongojs from 'mongojs'
import {
    success,
    internalServerError,
    badRequest,
    conflict,
    unAuthorized,
    valueMismatch
} from "../../config/responsetemplate";
import verifyotp from "../../helpers/verifyotp";
import sendsms from "../../helpers/sendsms";

export default (request, response) => {
    let query = {};
    let {
        data
    } = request.body;

    if (!validate(data, ["mobile", "otp"])) {
        badRequest(response)
    } else {
        // verifyotp(data.mobile, data.otp)
        new Promise((resolve, reject) => { resolve({ type: "success", "message": "otp_verified" }) })
            .then((resp) => {
                if (resp.type == "success" && resp.message == "otp_verified") {
                    data["createdOn"] = Date.now()
                    data["status"] = "created"
                    data["vendorId"]= mongojs.ObjectId(data.vendorId)
                    db.orders.insert(data, (err, doc) => {
                        if (err) {
                            internalServerError(response)
                        }
                        else {
                            // sendsms(data.mobile, `Thanks for using our services. Your order id is #${doc._id}. We will update you soon.`)
                            new Promise((resolve, reject) => { resolve({ type: "success", "message": "otp_verified" }) })
                                .then((data) => {
                                    success(response, {
                                        message: "order placed"
                                    })
                                }).catch((err) => {
                                    //sms has failed
                                    //think some other alternate to inform user
                                })

                        }
                    })

                }
                else {
                    valueMismatch(response, resp)
                }
            })
            .catch((err) => {
                console.log(err);
                internalServerError(err)
            })

    }
};
