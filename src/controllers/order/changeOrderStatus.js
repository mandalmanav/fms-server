
import db from '../../config/db'
import validate from '../../helpers/fieldValidation'

import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'
import isAuth from '../../helpers/isAuth';
import mongojs from 'mongojs'
export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!validate(data, ["id", "action"])) {
        badRequest(response)
    } else {
        if(data.action == "prepare"){
            query["acceptedAt"] = Date.now()
            query["status"] = "accepted"
        }
        else if(data.action == "deliver"){
            query["shippedAt"] = Date.now()
            query["status"] = "shipped"
        }
        else if(data.action == "complete"){
            query["deliveredAt"] = Date.now()
            query["status"] = data.status || "delivered"
        }
        isAuth(request).then((user) => {
           
            db.orders.update({
                _id:mongojs.ObjectId(data.id)
            }, {
                    $set: query
                }, (err, doc) => {
                    if (err) {
                        internalServerError(response)
                    }
                    else {
                        success(response, {nModified:doc.nModified})
                    }
                })
        })
            .catch((err) => {
                unAuthorized(response)
            })
    }



}

