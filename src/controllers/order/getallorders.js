
import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'
import isAuthed from '../../helpers/isAuth'
import isSuperAdmin from '../../helpers/isSuperAdmin'
export default (request, response) => {
    let query = {}
    let { data } = request.body
    // if (!isAuthed()) {
    //     unAuthorized(response)
    //     return
    // }
    if (!isSuperAdmin()) {
        forbidden(response)
        return
    }
    if (false) {
        
    }
    else {
        db.orders.aggregate([
            { $lookup:
              {
                from: 'vendors',
                localField: 'vendorId',
                foreignField: '_id',
                as: 'vendordetails'
              }
            }
          ]).toArray((err, doc)=> {
            if (err) {
                            internalServerError(response)
                        }
                        else {
                             success(response, doc)
                        }
          });
        
        
        // db.orders.find({}, (err, doc) => {
        //         if (err) {
        //             internalServerError(response)
        //         }
        //         else {
        //              success(response, doc)
        //         }
        //     })
    }
}