
import db from '../../config/db'
import validate from '../../helpers/fieldValidation'

import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'
import isAuth from '../../helpers/isAuth';
import mongojs from 'mongojs'
export default (request, response) => {
    let query = {}
    let { data } = request.body

    isAuth(request).then((user) => {
        query["vendorId"] = user._id;
        if(data.status.indexOf('All')!=-1){
            query["status"] ={
                $in:["delivered","undelivered","partially_delivered","cancelled","returned","other"]
            }
        }
        else{
            query["status"]={
                $in:data.status
            }
        }
        if(data.stDate){
            query["createdOn"]={
                $gte:Number(data.stDate),
                $lt:Number(data.endDate)
            }
        }
        if(data.searchOrderId){
            query["_id"]=mongojs.ObjectId(data.searchOrderId)
        }
       
        let limit = data.limit || 0;
        let offset = data.offset || 0
        db.orders.find(query).limit(limit).skip(offset).sort({ createdOn: -1 }, (err, docs) => {
            if (err) {
                internalServerError(response)
            }
            else {
                db.orders.count(query,(err_,count)=>{
                    if(!err_)
                    success(response, {
                        orders:docs,
                        count
                    })
                    else{
                        console.log(err_);
                        internalServerError(response)
                    }
                })
                
            }
        })
    })
        .catch((err) => {
            console.log(err);
            unAuthorized(response)
        })


}

