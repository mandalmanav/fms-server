
import db from '../../config/db'
import validate from '../../helpers/fieldValidation'

import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'
import isAuth from '../../helpers/isAuth';
import mongojs from 'mongojs'
export default (request, response) => {
    let query = {}
    let { data } = request.body

    isAuth(request).then((user) => {
        query["vendorId"] = user._id;
        query["status"] = data.status;
        
        db.orders.find(query).sort({ createdOn: -1 }, (err, docs) => {
            if (err) {
                internalServerError(response)
            }
            else {
                success(response, docs)
            }
        })
    })
        .catch((err) => {
            unAuthorized(response)
        })


}

