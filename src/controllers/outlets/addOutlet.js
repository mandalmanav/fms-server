import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";
import {
  success,
  internalServerError,
  badRequest,
  conflict,
  unAuthorized
} from "../../config/responsetemplate";

export default (request, response) => {
  let query = {};
  let {
    data
  } = request.body;
  isAuth(request)
    .then(user => {
      if (!validate(data, [
          "name",
          "mobile",
          "stations",
          "city",
          "items",
          'opensAt',
          "closesAt",
          "deliverySLA"
          

        ])) {
        badRequest(response);
      } else {
        let query = {...data}
        query["createdOn"] = Date.now();
        query["modifiedOn"] = Date.now();
        query["status"] = "active";
        query["vendorId"] = user._id;
        

        db.outlets.insert(query, (err, doc) => {
          if (err) {
            internalServerError(response);
          } else {
            success(response, {
              _id: doc._id
            });
          }
        });
      }
    })
    .catch(err => {
      unAuthorized(response);
    });
};
