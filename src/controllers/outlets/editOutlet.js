import mongojs from "mongojs";
import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";
import {
  success,
  internalServerError,
  badRequest,
  conflict,
  unAuthorized
} from "../../config/responsetemplate";

export default (request, response) => {
  let query = {};
  let { data } = request.body;
  isAuth(request)
    .then(user => {
      if (
        !validate(data, [
          "name",
          "mobile",
          "stations",
          "city",
          "items",
          'opensAt',
          "closesAt",
          "deliverySLA",
          "outletId"
        ])
      ) {
        badRequest(response);
      } else {
       data["modifiedOn"]= Date.now()

        db.outlets.update(
          {
            _id: mongojs.ObjectId(data["outletId"])
          },
          {
            $set: {
              ...data
            }
          },
          (err, doc) => {
            if (err) {

              internalServerError(response);
            } else {
              success(response, {"modified":doc.nModified});
            }
          }
        );
      }
    })
    .catch((err) => {
      console.log(err);
      unAuthorized(response);
    });
};
