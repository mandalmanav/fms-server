
import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";
import isSuperAdmin from "../../helpers/isSuperAdmin";
import mongojs from 'mongojs'
import {
  success,
  internalServerError,
  badRequest,
  conflict,
  unAuthorized
} from "../../config/responsetemplate";

export default (request, response) => {
  let query = {};
  let { data } = request.body;
    isAuth(request).then((user)=>{
      if (
        !validate(data, [
          "outletId"
        ])
      ) {
        badRequest(response);
      }
      else{
        db.outlets.find(
      
          {
            vendorId:user._id,
            _id: mongojs.ObjectId(data["outletId"])
          },
          (err, doc) => {
            console.log(err);
            if (err) {
              internalServerError(response);
            } else {
              success(response, doc[0]);
            }
          }
        );
      }
   
})
.catch((error)=>{
    unAuthorized(response)
})
 
};
