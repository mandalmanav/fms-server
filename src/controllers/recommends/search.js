
import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'

export default (request, response) => {
    let query = {}
    let { data } = request.body
    let responseData = {}
    if (!validate(data, ["stationCode", "key"])) {
        badRequest(response)
    }
    else {
        db.vendors.find({
            status: 'active',
            activeStations: data.stationCode,
            regName: {
                $regex: data.key, $options: 'i'
            }
        }, {
                _id: 1,
                regName: 1,
                opensAt: 1,
                closesAt: 1,
                imagePath: 1,
                nonVeg: 1,
                cuisines: 1,
                deliverySLA: 1
            }, (err, doc) => {
                if (err) {
                    internalServerError(response)
                }
                else {
                    responseData["vendors"] = doc

                    db.vendors.find({
                        status: 'active',
                        activeStations: data.stationCode,
                        cuisines: {
                            $regex: data.key, $options: 'i'
                        }
                    }, {
                            _id: 1,
                            regName: 1,
                            opensAt: 1,
                            closesAt: 1,
                            imagePath: 1,
                            nonVeg: 1,
                            cuisines: 1,
                            deliverySLA: 1
                        }, (error, cuisineDoc) => {
                            if (err) {
                                internalServerError(response)
                            }
                            else {
                                responseData["cuisines"] = cuisineDoc
                                db.vendors.find({
                                    status: 'active',
                                    activeStations: data.stationCode
                                }, {
                                        _id: 1
                                    },(__err,__doc)=>{
                                        if(__err){
                                            internalServerError(response)
                                        }
                                        else{
                                            let vIds = __doc.map((dc) => {
                                                return String(dc._id)
                                            })
                                            console.log(vIds);
                                            db.items.distinct("name", {
                                                name: {
                                                    $regex: data.key, $options: 'i'
                                                },
                                                vendorId: {
                                                    $in: vIds
                                                }
                                            }, (_error, _doc) => {
                                                responseData["items"] = _doc
                                                success(response, responseData)
                                            })
                                        }
                                    })

                               

                            }
                        })

                }
            })
    }
}

