import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import isAuthed from '../../helpers/isAuth'
import isSuperAdmin from '../../helpers/isSuperAdmin'
import { success, internalServerError, badRequest, conflict } from '../../config/responsetemplate'


export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!isAuthed()) {
        unAuthorized(response)
        return
    }
    if (!isSuperAdmin()) {
        forbidden(response)
        return
    }
    if (!validate(data, ["trainno", "name"])) {
        badRequest(response)
    }
    else {

                    db.trains.insert({
                        _id: data.trainno,
                        name: data.name,
                        code:data.code,
                        status: 'active'

                    }, (err, doc) => {
                        if (err) {
                            conflict(response)
                        }
                        else {
                            success(response, { _id: doc._id })
                        }
                    })
             

    }

}