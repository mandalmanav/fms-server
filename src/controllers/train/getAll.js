import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import isAuthed from '../../helpers/isAuth'
import isSuperAdmin from '../../helpers/isSuperAdmin'
import { success, internalServerError, badRequest, conflict } from '../../config/responsetemplate'


export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!isAuthed()) {
        unAuthorized(response)
        return
    }
    if (!isSuperAdmin()) {
        forbidden(response)
        return
    }
    if (false) {
      //  badRequest(response)
    }
    else {
       
        db.trains.find({}, (err, doc) => {
            if (err) {
                internalServerError(response)
            }
            else {
               success(response,doc)
                        
            }
        })

    }

}