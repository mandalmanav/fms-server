import mongojs from 'mongojs'
import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'
import isAuthed from '../../helpers/isAuth'
import isSuperAdmin from '../../helpers/isSuperAdmin'
export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!isAuthed()) {
        unAuthorized(response)
        return
    }
    if (!isSuperAdmin()) {
        forbidden(response)
        return
    }
    if (!validate(data, ["trainId","trainno","name"])) {
        badRequest(response)
    }
    else {
        let { name } = data
        db.trains.update({
            "_id": mongojs.ObjectId(data["trainId"])
        }, {
                $set:{
                    name
                }
            }, (err, doc) => {
                if (err) {
                    internalServerError(response)
                }
                else {
                     success(response, doc)
                }
            })
    }
}