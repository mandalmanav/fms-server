import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import { success, internalServerError ,badRequest } from '../../config/responsetemplate'


export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!validate(data,['email','name','mobile','password'])){
        badRequest(response)
    }
    else{
        query["email"] = data.email
        query["password"] = data.password
        query["mobile"] = data.mobile
        
        db.user.insert(query, (err, doc) => {
            if (err) {
                internalServerError(response)
            }
            else {
                success(response, doc)
            }
        })
    }
   
}