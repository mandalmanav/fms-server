import db from '../../config/db'

import { success, internalServerError } from '../../config/responsetemplate'

export default (request, response) => {
    let search = request.query.email || "."
    let pwd = request.query.password || "."
    let mobile = request.query.mobile || "."
    
    let limit  = Number(request.query.limit) || 0
    let offset = Number(request.query.offset) || 0
    let query = {
        $or:
            [
                {
                    email: {
                        $regex: search, $options: 'i'
                    }
                },
                {
                    password: {
                        $regex: pwd, $options: 'i'
                    }
                },
                {
                    mobile: {
                        $regex: mobile, $options: 'i'
                    }
                }
            ]


    }
    db.user.find().limit(limit).skip(offset, (err, docs) => {
        if (err) {
            internalServerError(response, err)
        }
        else {
            success(response, docs)
        }
    })
}