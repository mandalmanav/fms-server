import db from '../../config/db'

import { success, internalServerError } from '../../config/responsetemplate'


export default (request, response) => {
    let obj = {}
    let { data } = request.body
    obj["name"] = data.name
    obj["address"] = data.address
    obj["mobile"] = data.mobile

    db.vendors.insert(obj, (err, doc) => {
        if (err) {
            internalServerError(response)
        }
        else {
            success(response, doc)
        }
    })
}