import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import { success, internalServerError, badRequest, conflict } from '../../config/responsetemplate'
import generateOtp from '../../helpers/generateotp'

export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!validate(data, ['email', 'name', 'mobile', 'password', 'regName', 'address'])) {
        badRequest(response)
    }
    else {
        query["email"] = data.email
        query["password"] = data.password
        query["mobile"] = data.mobile
        query["address"] = data.address
        query["regName"] = data.regName
        query["createdOn"] = Date.now()
        query["modifiedOn"] = Date.now()
        query["status"] = "pending",
        query["role"] = "vendor"

        db.vendors.find({
            $or: [{
                email: data.email
            }
                ,
            {
                regName: data.regName
            }
            ]

        }, (err, doc) => {
            if (err) {
                internalServerError(response)
            }
            else {
                if (doc.length) {
                    conflict(response)
                } else {
                    generateOtp(`91${data.mobile}` , "Your%20OTP%20for%20registration%20is%20##OTP## ")
                    .then((otpresponse)=>{
                        
                        db.vendors.insert(query, (err, doc) => {
                            if (err) {
                                internalServerError(response)
                            }
                            else {
                                success(response, { _id: doc._id })
                            }
                        })
                    })
                    .catch((reject)=>{

                        console.log("rejected", reject);
                        internalServerError(response,{
                            message:"error in otp"
                        })
                    })
                    
                }
            }
        })

    }

}