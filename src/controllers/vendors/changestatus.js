import mongojs from 'mongojs'
import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'
import isAuth from '../../helpers/isAuth'
import isSuperAdmin from '../../helpers/isSuperAdmin'
export default (request, response) => {
    let query = {}
    let { data } = request.body
   

   
    if (!validate(data, ["vendorId","status"])) {
        badRequest(response)
    }
    else {
        isAuth(request)
        .then((user)=>{
            if(user._id == data.vendorId){
                query["modifiedOn"] = Date.now()
                query["status"] = data.status
                db.vendors.update({
                    "_id": mongojs.ObjectId(data["vendorId"])
                }, {
                        $set:query
                    }, (err, doc) => {
                        if (err) {
                            internalServerError(response)
                        }
                        else {
                             success(response, doc)
                        }
                    })
            }else{
                if(isSuperAdmin()){
                    query["approvedOn"] = Date.now()
                    query["status"] = data.status || approved
                    db.vendors.update({
                        "_id": mongojs.ObjectId(data["vendorId"])
                    }, {
                            $set:query
                        }, (err, doc) => {
                            if (err) {
                                internalServerError(response)
                            }
                            else {
                                 success(response, doc)
                            }
                        })
                }
               else
                forbidden(response)
            }
        })
        .catch((err)=>{
            unAuthorized(response)
        })
       
       
    }
}