import mongojs from "mongojs";
import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import {
  success,
  internalServerError,
  badRequest,
  conflict,
  forbidden,
  unAuthorized
} from "../../config/responsetemplate";
import isAuthed from "../../helpers/isAuth";
import isSuperAdmin from "../../helpers/isSuperAdmin";

export default (request, response) => {
  let query = {};
  let { data } = request.body;
  isAuthed(request)
    .then((user) => {
        data["channel"] = "vendor-app"
        data["orderedBy"] = user._id
        data["createdOn"] = Date.now()
        db.orders.insert(data,(err,doc)=>{
            if(err){
                internalServerError(response)
            }
            else{
                success(response,doc)
            }
        })
    })
    .catch((err) => {
      console.log(err);
      unAuthorized(response);
    });
};
