
import db from '../../config/db'
import validate from '../../helpers/fieldValidation'

import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'
import isAuth from '../../helpers/isAuth';
import mongojs from 'mongojs'
export default (request, response) => {
    let query = {}
    let { data } = request.body
  isAuth(request).then((user)=>{
    db.vendors.find({
        _id:mongojs.ObjectId(user._id)
     }, (err, doc) => {
             if (err) {
                 internalServerError(response)
             }
             else {
                delete doc[0].password
                 success(response, doc[0])
             }
         })
  })
  .catch((err)=>{
    unAuthorized(response)
  })
       
    
}

