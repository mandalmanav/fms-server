
import db from '../../config/db'
import validate from '../../helpers/fieldValidation'
import { success, internalServerError, badRequest, conflict, forbidden, unAuthorized } from '../../config/responsetemplate'
export default (request, response) => {
    let query = {}
    let { data } = request.body
    if (!validate(data, ["stationCode"])) {
        badRequest(response)
    }
    else {
        db.vendors.find({
            status: 'active',
            activeStations:data.stationCode
            
        }, {
                _id: 1,
                regName: 1,
                opensAt: 1,
                closesAt: 1,
                imagePath: 1,
                nonVeg: 1,
                cuisines: 1,
                deliverySLA: 1
            }, (err, doc) => {
                if (err) {
                    internalServerError(response)
                }
                else {
                    success(response, doc)
                }
            })
    }
}

