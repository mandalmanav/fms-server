import mongojs from "mongojs";
import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import {
  success,
  internalServerError,
  badRequest,
  conflict,
  forbidden,
  unAuthorized
} from "../../config/responsetemplate";
import isAuthed from "../../helpers/isAuth";
import isSuperAdmin from "../../helpers/isSuperAdmin";

export default (request, response) => {
  let query = {};
  let { data } = request.body;
  isAuthed(request)
    .then((user) => {
      // query["regName"] = data.regName;
      // query["name"] = data.regName;
      // query["mobile"] = data.mobile;
      // query["modifiedOn"] = Date.now();
      // query["opensAt"] = data.opensAt || 0;
      // query["closesAt"] = data.closesAt || 23.59;
      // query["imagePath"] = data.imagePath || "/noimage.png";
      // query["nonveg"] = data.nonveg || false;
      // query["cuisines"] = data.cuisines || []; // type of cuisines
      // query["deliverySLA"] = data.deliverySLA || 60; // mins
      // query["activeStations"] = data.activeStations || "";
      // query["minimumAmount"] = data.minimumAmount || 0;
      // query["maximumAmount"] = data.maximumAmount || 10000;
      data["modifiedOn"] = Date.now()
      delete data["_id"]
      db.vendors.update(
        {
          _id: mongojs.ObjectId(user._id)
        },
        {
          $set: data
        },
        (err, doc) => {
          if (err) {
            internalServerError(response);
          } else {
            success(response, doc);
          }
        }
      );
    })
    .catch((err) => {
      console.log(err);
      unAuthorized(response);
    });
};
