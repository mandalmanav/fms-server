import Axios from 'axios'
import {msg91apikey} from '../config/index'
export default (mobile,message) =>{
    return new Promise((resolve,reject) => {
        var http = require("https");

        var options = {
          "method": "POST",
          "hostname": "control.msg91.com",
          "port": null,
          "path": `/api/sendotp.php?&otp_length=4&otp_expiry=5&sender=YOWZAT&mobile=${mobile}&authkey=${msg91apikey}`,
          "headers": {}
        };
        
        var req = http.request(options, function (res) {
          var chunks = [];
        
          res.on("data", function (chunk) {
            chunks.push(chunk);
          });
          res.on("error", function (error) {
            console.log(error);
            reject("error in sending otp")
          });
          res.on("end", function () {
            var body = Buffer.concat(chunks);
          
            resolve(body.toString())
          });
        });
        
        req.end();
        
    })
}
