import jwt from 'jsonwebtoken'
import { tokenConfig } from '../config'
import { unAuthorized } from '../config/responsetemplate'
export default (request)=>{
    let token = request.headers["authorization"]
  return new Promise((resolve,reject)=>{
    jwt.verify(token, tokenConfig.token.secret, (err, decoded)=> {
       
        if(err){
            reject(true)
        }
        else {
            resolve(decoded)
        }
    })
  })
   
}