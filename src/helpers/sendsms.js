var http = require("https");
import {msg91apikey} from '../config/index' 
export default (mobile,message) =>{
    return new Promise((resolve,reject)=>{

        var options = {
            "method": "POST",
            "hostname": "api.msg91.com",
            "port": null,
            "path": "/api/v2/sendsms",
            "headers": {
              "authkey": msg91apikey,
              "content-type": "application/json"
            }
          };
          
          var req = http.request(options, function (res) {
            var chunks = [];
            res.on('error',(err)=>{
                reject(err)
            })
            res.on("data", function (chunk) {
              chunks.push(chunk);
            });
          
            res.on("end", function () {
              var body = Buffer.concat(chunks);
              resolve(JSON.parse(body.toString()));
            });
          });
          
          req.write(JSON.stringify({ sender: 'YOWZAT',
            route: '4',
            // country: '91',
            sms: 
             [ { message, to: [Number(mobile) ] } ] 
            }));
          req.end()
    })
}
    