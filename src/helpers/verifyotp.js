import Axios from "axios";
import { msg91apikey } from "../config/index";
export default (mobile, otp) => {
  return new Promise((resolve, reject) => {
    var qs = require("querystring");
    var http = require("https");

    var options = {
      method: "POST",
      hostname: "control.msg91.com",
      port: null,
      path: `/api/verifyRequestOTP.php?authkey=${msg91apikey}&mobile=${mobile}&otp=${otp}`,
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    };

    var req = http.request(options, function(res) {
      var chunks = [];

      res.on("data", function(chunk) {
        chunks.push(chunk);
      });
      res.on("error", function() {
        reject("rejected by sms gateway");
      });
      res.on("end", function() {
        var body = Buffer.concat(chunks);
        console.log(body.toString());
        resolve(JSON.parse(body.toString()));
      });
    });

    req.write(qs.stringify({}));
    req.end();
  });
};
