import passport from 'passport'
import loginController from '../controllers/auth/logincontroller'
import resetpassword from '../controllers/auth/resetpassword';
import verifyotpcontroller from '../controllers/auth/verifyotpcontroller';
import changepassword from '../controllers/auth/changepassword';
export default (app) => {
    app.use(passport.initialize())
    passport.serializeUser((user, done) => done(null, user));
    app.post('/auth/resetpassword',resetpassword)
    app.post('/auth/changepassword',changepassword)
    app.post('/auth/login' , loginController)
    app.get('/auth/google/redirect', passport.authenticate('google'), (req, res, profile) => {
        console.log(profile);
        res.redirect('http://www.google.com')
    })
    app.get('/auth/google', passport.authenticate('google', { scope: ['profile'] }));
    app.post('/verify-otp',verifyotpcontroller)
    return app
}

