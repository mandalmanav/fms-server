import generateOtp from "../controllers/core/generateOtp";
import getStations from "../controllers/core/getStations";
import getVendors from "../controllers/core/getVendors";
import getItemsByVendor from "../controllers/core/getItemsByVendor";

export default (app )=>{
    app.get('/core/getStations/:trainNo' ,getStations )
    app.post('/core/generateOTP' ,generateOtp )
    app.get('/core/getVendors/:code',getVendors)
    app.get('/core/items/:vendorId',getItemsByVendor)
    return app
}