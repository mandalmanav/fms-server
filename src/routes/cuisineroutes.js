import addCuisine from '../controllers/masters/Cuisine/addcuisine'
import editcuisine from '../controllers/masters/Cuisine/editcuisine'
import getallcuisine from '../controllers/masters/Cuisine/getallcuisine'
import getcuisinebyid from '../controllers/masters/Cuisine/getcuisinebyid'

export default (app )=>{
    app.post('/masters/Cuisine/addcuisine' ,addCuisine )
    app.post('/masters/Cuisine/editcuisine' ,editcuisine )
    app.get('/masters/Cuisine/getallcuisine',getallcuisine)
    app.post('/masters/Cuisine/getcuisinebyid',getcuisinebyid)
    return app
}