
import db from "../../config/db";
import validate from "../../helpers/fieldValidation";
import isAuth from "../../helpers/isAuth";
import isSuperAdmin from "../../helpers/isSuperAdmin";

import {
  success,
  internalServerError,
  badRequest,
  conflict,
  unAuthorized
} from "../../config/responsetemplate";

export default (request, response) => {
  let query = {};
  let { data } = request.body;
isAuth(request).then((user)=>{
    db.outlets.find(
        {
          vendorId: user._id
        },
        (err, doc) => {
          if (err) {
            internalServerError(response);
          } else {
            success(response, doc);
          }
        }
      );
})
.catch((error)=>{
    unAuthorized(response)
})
 
};
