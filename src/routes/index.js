import vendorRoutes from './vendorroutes'
import authRoutes from './authroutes'
import userRoutes from './userRoutes'
import train from './train'
import items from './itemroutes'
import outlets from './outlets'
import core from './core';
import order from './order';
import fileupload from './fileuploadroutes'
import cuisineroutes from './cuisineroutes'
export default (app) => {
    app.get('/health', (request, response) => {
        response.status(200).send({
            status: 'ok'
        })
    })
    
    vendorRoutes(app)
    authRoutes(app)
    userRoutes(app)
    train(app)
    outlets(app)
    items(app)
    core(app)
    order(app)
    cuisineroutes(app)
    fileupload(app)
    return app
}
