
import additem from '../controllers/items/additem';
import edit from '../controllers/items/edititem';
import getitemsbyvendors from '../controllers/items/getitemsbyvendors';
export default (app )=>{
    app.post('/items/add' ,additem )
    app.post('/items/update' ,edit )
    app.post('/items/getItemsbyvendor' ,getitemsbyvendors )
    return app
}