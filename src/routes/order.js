import addorder from "../controllers/order/addorder";
import getordersbyvendor from "../controllers/order/getordersbyvendor";
import changeOrderStatus from "../controllers/order/changeOrderStatus";
import getorderreport from "../controllers/order/getorderreport";
import getallorderreport from "../controllers/order/getallorders";
export default (app )=>{
    app.post('/order/add' ,addorder )
    app.post('/getorders',getordersbyvendor)
    app.post('/order/changestatus',changeOrderStatus)
    app.post('/order/report',getorderreport)
    app.get('/superadmin/ordersreport',getallorderreport)
    return app
}