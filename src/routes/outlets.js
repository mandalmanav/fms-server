
import addOutlet from '../controllers/outlets/addOutlet';
import editOutlet from '../controllers/outlets/editOutlet';
import getOutlets from '../controllers/outlets/getOutlets'
import getOutletById from '../controllers/outlets/getOutletById';
import getOutletByVendor from '../controllers/outlets/getOutletByVendor';
export default (app )=>{
    app.post('/outlets/add' ,addOutlet )
    app.post('/outlets/update' ,editOutlet )
    app.get('/outlets' ,getOutlets )
    app.post('/outletbyid' ,getOutletById )
    app.post('/outletbyvendor' ,getOutletByVendor )
    return app
}