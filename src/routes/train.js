import addTrainController from '../controllers/train/add'
import getTrains from '../controllers/train/getAll'
import updateTrainController from '../controllers/train/update'
import getTrainNames from '../controllers/train/gettrainnames'

export default (app )=>{
    app.post('/trains/add' ,addTrainController )
    app.post('/trains/update' ,updateTrainController )
    app.get('/trains/all',getTrains)
    app.get('/trains/active',getTrainNames)
    return app
}