import addUserController from '../controllers/user/add'
import getUserController from '../controllers/user/getuser'
export default (app )=>{
    app.post('/user/create' ,addUserController )
    app.get('/user',getUserController)
    return app
}