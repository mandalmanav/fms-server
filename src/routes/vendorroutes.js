import getAllVendorsController from '../controllers/vendors/getallvendorscontroller'
import applyForVendorController from '../controllers/vendors/applyforvendorcontroller' 
import changestatus from '../controllers/vendors/changestatus'
import updateVendor from '../controllers/vendors/updatevendorcontroller'
import getvendors from '../controllers/vendors/getvendors';
import search from '../controllers/recommends/search';
import getVendorById from '../controllers/vendors/getVendorById';
import createOrderByVendor from '../controllers/vendors/createOrderByVendor';
import moment from 'moment'
export default (app) => {
    console.log(moment().format('HH:mm'));
    app.get('/vendor/all', getAllVendorsController)
    app.get('/vendor/details', getVendorById)
    app.post('/vendor/getvendors', getvendors)
    app.post("/vendor/apply",applyForVendorController)
    app.post("/vendor/changestatus",changestatus)
    app.post("/vendor/updatevendor",updateVendor)
    app.post("/vendor/createorder",createOrderByVendor)
    app.post("/search",search)
    return app
}
    