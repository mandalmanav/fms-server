import express from 'express';
import appRoutes from './routes'
import passport from 'passport'
import authroutes from './routes/authroutes';
import cors from 'cors'
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger/index';
const app = express()
const port = process.env.PORT || 8888;
const bodyParser = require('body-parser')


require('./config/passport-setup')
var morgan = require('morgan')
var fs = require('fs')
var path = require('path')
var rfs = require('rotating-file-stream')
var logDirectory = path.join(__dirname, '../logs')
var accessLogStream = rfs('access.log', {
  interval: '1d',
  path: logDirectory
})
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)
process.env.DEV ? app.use(morgan(':method      :url   [:date[clf]]     :response-time ms')) : app.use(morgan(':method      :url   [:date[clf]]     :response-time ms', { stream: accessLogStream }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors())
app.use(express.static('public'));



///////////////


app.get('/', function (request, response) {
  response.sendFile(__dirname + '/public/index.html');
});

app.get("/success", function (request, response) {
  response.sendFile(__dirname + '/public/success.html');
});

app.get("/error", function (request, response) {
  response.sendFile(__dirname + '/public/error.html');
});


////////////////
process.env.DEV ? app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument)) : null;
appRoutes(app)
// authroutes(app)
app.listen(port, () => console.log(`listening on port ${port}!`))
