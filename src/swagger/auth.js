export default {
    "auth/login": {
        "post": {
            "tags": [
                "auth"
            ],
            "summary": "validate and generate token",
            "description": "",
            "operationId": "login",
            "consumes": ["application/json"],
            "produces": ["application/json"],
            "parameters": [{
                "in": "body",
                "name": "email",
                "required": true

            },
            {
                "in": "body",
                "name": "password",
                "required": true

            }
            ]
        }
    }
}