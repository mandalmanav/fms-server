import trains from './trains'
import auth from './auth'
import vendors from './vendors'
import item from './item'
export default {

    "swagger": "2.0",
    "info": {
        "description": "This is FMS api documentation",
        "version": "1.0.0",
        "title": "FMS"

    },
    "host": "localhost:8888",

    "tags": [
        {
            "name": "vendors",
            "description": "Used to create/manage vendors"

        },
        {
            "name": "trains",
            "description": "Used to create/manage trains master"

        },
        {
            "name": "auth",
            "description": "Used to manage authorizations"
        }
    ],
    "schemes": [
        "http"
    ],
    "paths": {
        ...trains,
        ...auth,
        ...vendors,
        ...item
    }
}