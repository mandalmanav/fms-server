export default {
    "trains/add": {
        "post": {
            "tags": [
                "trains"
            ],
            "summary": "Add a train detail",
            "description": "",
            "operationId": "addtrain",
            "consumes": ["application/json"],
            "produces": ["application/json"],
            "parameters": [{
                "in": "body",
                "name": "trainno",
                "required": true

            },
            {
                "in": "body",
                "name": "name",
                "required": true

            },
            {
                "in": "body",
                "name": "code",
                "required": false

            }
            ]
        }
    }
}

