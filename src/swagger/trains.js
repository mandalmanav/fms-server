export default {

    "/trains/all": {
        "get": {
            "tags": [
                "trains"
            ],
            "summary": "Fetch all trains available (active / inactive both)",
            "operationId": "all",

            "produces": [
                "application/json"
            ],

            "responses": {
                "200": {
                    "description": "Success"
                }
            }
        }

    },
    "/trains/active": {
        "get": {
            "tags": [
                "trains"
            ],
            "summary": "Fetch all trains available with status active",
            "operationId": "active",

            "produces": [
                "application/json"
            ],

            "responses": {
                "200": {
                    "description": "Success"
                }
            }
        }

    },
    "trains/add": {
        "post": {
            "tags": [
                "trains"
            ],
            "summary": "Add a train detail",
            "description": "",
            "operationId": "addtrain",
            "consumes": ["application/json"],
            "produces": ["application/json"],
            "parameters": [{
                "in": "body",
                "name": "trainno",
                "required": true

            },
            {
                "in": "body",
                "name": "name",
                "required": true

            },
            {
                "in": "body",
                "name": "code",
                "required": false

            }
            ]
        }
    },
    "trains/update": {
        "post": {
            "tags": [
                "trains"
            ],
            "summary": "update a train detail",
            "description": "",
            "operationId": "addtrain",
            "consumes": ["application/json"],
            "produces": ["application/json"],
            "parameters": [{
                "in": "body",
                "name": "trainId",
                "required": true

            },
            {
                "in": "body",
                "name": "trainno",
                "required": true

            },
            {
                "in": "body",
                "name": "name",
                "required": false

            }
            ]
        }
    }



}