export default {
    "/vendor/all": {
        "get": {
            "tags": [
                "vendors"
            ],
            "summary": "Fetch all vendors (active / inactive both)",
            "operationId": "all",

            "produces": [
                "application/json"
            ],

            "responses": {
                "200": {
                    "description": "Success"
                }
            }
        }

    },
    "vendor/apply": {
        "post": {
            "tags": [
                "vendors"
            ],
            "summary": "Request to become a vendor",
            "description": "",
            "operationId": "apply",
            "consumes": ["application/json"],
            "produces": ["application/json"],
            "parameters": [{
                "in": "body",
                "name": "name",
                "required": true

            },
            {
                "in": "body",
                "name": "email",
                "required": true

            },
            {
                "in": "body",
                "name": "mobile",
                "required": true

            },
            {
                "in": "body",
                "name": "address",
                "required": true

            }
                ,
            {
                "in": "body",
                "name": "regName",
                "required": true

            }
                ,
            {
                "in": "body",
                "name": "password",
                "required": true

            }
            ]
        }
    },
    "vendor/changestatus": {
        "post": {
            "tags": [
                "vendors"
            ],
            "summary": "change status of vendor to active or declined or anything else",
            "description": "",
            "operationId": "changestatus",
            "consumes": ["application/json"],
            "produces": ["application/json"],
            "parameters": [{
                "in": "body",
                "name": "vendorId",
                "required": true

            },
            {
                "in": "body",
                "name": "status",
                "required": false,
                default: "active"

            }
            ]
        }
    },
    "vendor/updatevendor": {
        "post": {
            "tags": [
                "vendors"
            ],
            "summary": "Request to become a vendor",
            "description": "",
            "operationId": "apply",
            "consumes": ["application/json"],
            "produces": ["application/json"],
            "parameters": [{
                "in": "body",
                "name": "vendorId",
                "required": true

            },
            {
                "in": "body",
                "name": "regName",
                "required": true

            }
                ,
            {
                "in": "body",
                "name": "openAt",
                "required": false,
                default: 0

            },
            {
                "in": "body",
                "name": "closesAt",
                "required": false,
                default: 11.59

            },
            {
                "in": "body",
                "name": "imagePath",
                "required": false

            }

            ]
        }
    }
}